# Project's root directory.
CFG_PROJECT_ROOT := /home/shikashi/devel/famaf/md2/labs/lab1

# Directory where to put compiled files.
CFG_BUILD_OUTPUT := build

# static-debug, static-release
CFG_DEFAULT_TARGET := static-release
