/*
 * graph.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <dragon/common.h>
#include <dragon/graph/edge.h>

/**
 * Callback function used during vertex iteration, where:
 *   x      Label of the vertex being visited.
 *   data   Opaque pointer reserved for the user's contextual data.
 * It should return true to stop the current iteration and false to let it
 * continue.
 */
typedef bool (*graph_foreach_vertex_f)(u32 x, void* data);

/**
 * Callback function used during edge iteration, where:
 *   x      Source vertex's label of the edge being visited.
 *   y      Destination vertex's label of the edge being visited.
 *   e      Data associated with the edge being visited.
 *   data   Opaque pointer reserved for the user's contextual data.
 * It should return true to stop the current iteration and false to let it
 * continue.
 */
typedef bool (*graph_foreach_edge_f)(u32 x, u32 y, edge_t e, void* data);

/**
 * Callback function used during edge folding, where:
 *   x      Source vertex's label of the edge being visited.
 *   y      Destination vertex's label of the edge being visited.
 *   e      Data associated with the edge being visited.
 *   base   Folding partial result.
 *   data   Opaque pointer reserved for the user's contextual data.
 * The returned value will be folded into the following edges.
 */
typedef u64 (*graph_foldl_edge_f)(u32 x, u32 y, edge_t e, u64 base, void* data);

typedef struct graph* graph_t;

/**
 * Constructor.
 *
 * Post: graph_create() != NULL
 */
graph_t graph_create(void);

/**
 * Destructor.
 *
 * Pre: g != NULL
 */
void graph_destroy(graph_t g);

/**
 * Return the amount of vertices in the graph.
 *
 * Pre: g != NULL
 */
size_t graph_vertices(graph_t g);

/**
 * Return the amount of edges in the graph.
 *
 * Pre: g != NULL
 */
size_t graph_edges(graph_t g);

/**
 * Return true if vertex 'v' exists in the graph, otherwise return false.
 *
 * Pre: g != NULL
 */
bool graph_vertex_exists(graph_t g, u32 v);

/**
 * Return true if edge 'x' to 'y' exists in the graph, otherwise return false.
 *
 * Pre: g != NULL && graph_vertex_exists(x) && graph_vertex_exists(y)
 */
bool graph_edge_exists(graph_t g, u32 x, u32 y);

/**
 * Add to the graph a new vertex with label 'v'.
 * Return the index corresponding to the newly added vertex.
 *
 * Pre: g != NULL && !graph_vertex_exists(g, v)
 */
void graph_new_vertex(graph_t g, u32 v);

/**
 * Add to the graph a new directed edge from vertex labeled 'x' to vertex
 * labeled 'y' and with capacity 'cap'.
 * If any of 'x' and 'y' don't exist, they will be added first.
 * Return the index corresponding to the newly added edge.
 *
 * Pre: g != NULL && an edge from 'x' to 'y' doesn't exist
 */
void graph_new_directed_edge(graph_t g, u32 x, u32 y, u32 cap);

/**
 * Return the data associated with edge 'x' to 'y'.
 *
 * Pre: g != NULL && graph_edge_exists(g, x, y)
 * Post: graph_directed_edge(g, x, y) != NULL
 */
edge_t graph_directed_edge(graph_t g, u32 x, u32 y);

/**
 * Call function 'f' for each vertex in the graph.
 * Return true if the iteration was interrupted due to a request from 'f'.
 * Otherwise return false.
 *
 * Pre: g != NULL && f != NULL
 */
bool graph_foreach_vertex(graph_t g, graph_foreach_vertex_f f, void* data);

/**
 * Call function 'f' for each edge in the graph. The opaque pointer 'data' is
 * passed along to 'f'.
 * Return true if the iteration was interrupted due to a request from 'f'.
 * Otherwise return false.
 *
 * Pre: g != NULL && f != NULL
 */
bool graph_foreach_edge(graph_t g, graph_foreach_edge_f f, void* data);

/**
 * Call function 'f' for each one of 'v's forward neighbours. The opaque
 * pointer 'data' is passed along to 'f'.
 * Return true if the iteration was interrupted due to a request from 'f'.
 * Otherwise return false.
 *
 * Pre: g != NULL && f != NULL && graph_vertex_exists(g, v)
 */
bool graph_foreach_edge_forward(graph_t g, u32 v, graph_foreach_edge_f f,
        void* data);

/**
 * Call function 'f' for each one of 'v's backward neighbours. The opaque
 * pointer 'data' is passed along to 'f'.
 * Return true if the iteration was interrupted due to a request from 'f'.
 * Otherwise return false.
 *
 * Pre: g != NULL && f != NULL && graph_vertex_exists(g, v)
 */
bool graph_foreach_edge_backward(graph_t g, u32 v, graph_foreach_edge_f f,
        void* data);

/**
 * Left-fold all of 'v's forward neighbours using 'f' as operator and 'base'
 * as initial partial result. The opaque pointer 'data' is passed along to 'f'.
 * Return the result of the folding.
 *
 * Pre: g != NULL && f != NULL && graph_vertex_exists(g, v)
 */
u64 graph_foldl_edge_forward(graph_t g, u32 v, graph_foldl_edge_f f, u64 base,
        void* data);

/**
 * Left-fold all of 'v's backward neighbours using 'f' as operator and 'base'
 * as initial partial result. The opaque pointer 'data' is passed along to 'f'.
 * Return the result of the folding.
 *
 * Pre: g != NULL && f != NULL && graph_vertex_exists(g, v)
 */
u64 graph_foldl_edge_backward(graph_t g, u32 v, graph_foldl_edge_f f, u64 base,
        void* data);

#endif
