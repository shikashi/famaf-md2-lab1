#ifndef _HASHMAP_BFS_H_
#define _HASHMAP_BFS_H_

#include <hashmap/hashmap_pod_macro/hashmap_pod_macro.h>
#include <dragon/common.h>
#include <dragon/ordered_set/bfs_data.h>

DECLARE_HASHMAP_POD(bfs, u32, struct bfs_data)

#endif
