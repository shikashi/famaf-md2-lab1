/*
 * ordered_set.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 * Copyright (c) 2013, Agustin Capello <aac0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _ORDERED_SET_H_
#define _ORDERED_SET_H_

#include <dragon/common.h>
#include <dragon/ordered_set/bfs_data.h>

/**
 * ordered_set
 *
 * The ordered set ADT represents a set whose elements can be iterated through
 * in a certain order. In addition, it acts as a (key, value) map so that
 * each element (key) in the set can have associated data (value).
 *
 * The iteration follows the order in which the elements were added to the set.
 * This means that if an algorithm adds elements to the set during an iteration,
 * those elements will be visited during that same iteration. Visited elements
 * are never re-visited, unless you rewind the iterator, which is internal.
 *
 * The last remark implies that there can be only one iteration of the set at
 * any given time, this is a limitation of the current implementation.
 *
 * The behavior of this ordered set makes it suitable for the implementation
 * of work queues (such as the one used in BFS traversal).
 */

typedef u32 set_elem_t;

typedef struct bfs_data* set_data_t;

typedef struct ordered_set* ordered_set_t;

/**
 * Constructor.
 * Construct an ordered set with initial size 'size'.
 *
 * Post: ordered_set_create(size) != NULL
 */
ordered_set_t ordered_set_create(size_t size);

/**
 * Destructor.
 *
 * Pre: os != NULL
 */
void ordered_set_destroy(ordered_set_t os);

/**
 * Append element 'el' to the set, only if it doesn't already exist. Return a
 * pointer to its associated data.
 * Note that this function can resize the set, potentially invalidating other
 * pointers into the set.
 *
 * Pre: os != NULL && el != NULL
 * Post: ordered_set_append(os, el) != NULL
 */
set_data_t ordered_set_append(ordered_set_t os, set_elem_t el);

/**
 * Return the amount of elements in the set.
 *
 * Pre: os != NULL
 */
size_t ordered_set_size(ordered_set_t os);

/**
 * Remove all elements from the set.
 *
 * Pre: os != NULL
 */
void ordered_set_clear(ordered_set_t os);

/**
 * If element 'el' exists in the set, return a pointer to its associated data.
 * Otherwise return NULL.
 *
 * Pre: os != NULL
 */
set_data_t ordered_set_find(ordered_set_t os, set_elem_t el);

/**
 * Rewind the set's iterator so that the next visited element will be the
 * first, if one exists.
 *
 * Pre: os != NULL
 */
void ordered_set_rewind(ordered_set_t os);

/**
 * If the iteration has not visited all elements yet, return a pointer to the
 * data associated with the current element. Otherwise, return NULL.
 *
 * Pre: os != NULL
 */
set_data_t ordered_set_curr(ordered_set_t os);

/**
 * If the iteration has not visited all elements yet, return a pointer to the
 * data associated with the current element and advance the (iternal) iterator.
 * Otherwise, return NULL.
 *
 * Pre: os != NULL
 */
set_data_t ordered_set_next(ordered_set_t os);

/**
 * Dump the set to stdout.
 * For debugging only.
 *
 * Pre: os != NULL
 */
void ordered_set_dump(ordered_set_t os);

#endif
