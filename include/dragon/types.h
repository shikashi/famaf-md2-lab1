#ifndef _DRAGON_TYPES_H_
#define _DRAGON_TYPES_H_

#include <stdint.h>
#include <stdbool.h>

typedef uint32_t u32;

typedef uint64_t u64;  /* __uint64_t */

#endif
