#ifndef _TUPLE_H
#define _TUPLE_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


typedef struct _tuple_t *tuple_t;


#define FORWARD true
#define BACKWARD false
#define MIN(a,b) (((a)<(b))?(a):(b))

tuple_t tuple_from_vertex(vertex_t vertex, tuple_t ancestor, u32 epsilon, bool direction);
/*
 * Build a new tuple from the given index and data using references to them.
 *
 * Do NOT free index and data after creating the tuple, but only through
 * tuple_destroy.
 *
 * PRE: "vertex" must be not NULL.
 */

tuple_t tuple_destroy(tuple_t t);
/*
 * Free the memory allocated by given tuple, as well as the respective
 * index and data. Set 't' to NULL.
 *
 * PRE: 't' must be not NULL.
 */

vertex_t tuple_vertex(tuple_t t);
/*
 * Return a reference to the first tuple element.
 *
 * PRE: 't' must be not NULL.
 */

tuple_t tuple_anc(tuple_t t);
/*
 * Return a reference to the second tuple element.
 *
 * PRE: 't' must be not NULL.
 */

u32 tuple_eps(tuple_t t);

/*
 * Return a reference to the third tuple element.
 *
 * PRE: 't' must be not NULL.
 */

bool tuple_dir(tuple_t t);
/*
 * Return a reference to the fourth tuple element.
 *
 * PRE: 't' must be not NULL.
 */
bool tuple_has_anc(tuple_t t);
/*
 * Return a false if t is the source else true.
 *
 * PRE: 't' must be not NULL.
 */
#endif
