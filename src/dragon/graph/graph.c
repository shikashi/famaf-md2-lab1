/*
 * graph.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <stdlib.h>
#include <assert.h>

#include <dragon/graph/graph.h>
#include <dragon/graph/hashmap_edge.h>
#include <dragon/graph/hashmap_neighbours.h>

/**
 * INITIAL_EDGE_ALLOC is the initial size of the edge hashmaps (forward and
 * backward) for each vertex.
 * It should be close to the average vertex degree (valency) in the graph.
 * TODO: Make this a dynamic value.
 * Note that for a graph with lots vertices, relatively small increases in
 * this value can result in very noticeable increases in memory consumption.
 * (TODO: hashmap_pod_fit() would be very useful here.)
 */
#define INITIAL_EDGES_ALLOC 16

struct graph {
    hashmap_neighbours_t neighbours;
    size_t edges;
};

graph_t graph_create(void)
{
    graph_t g = malloc(sizeof(*g));
    assert(g != NULL);

    g->edges = 0;
    g->neighbours = hashmap_neighbours_create(u32_hash, COMMON_VERTICES_ALLOC);

    return g;
}

/**
 * We're using a hashmap_pod (a general hashmap is not yet implemented), so
 * manually destroy the objects contained.
 * Note that both forward and backward maps are linked to the same objects,
 * so we only need to traverse one of them here.
 */
static void graph_adhoc_edges_destroyer(const u32* v, neighbours_t ngb)
{
    hashmap_edge_iterator_t it;
    for (it = hashmap_edge_iterator_begin(ngb->forward);
                !hashmap_edge_iterator_end(ngb->forward, it);
                        it = hashmap_edge_iterator_next(ngb->forward, it)) {
        edge_destroy(*hashmap_edge_iterator_at(ngb->forward, it));
    }

    hashmap_edge_destroy(ngb->forward);
    hashmap_edge_destroy(ngb->backward);

    (void)v;
}

void graph_destroy(graph_t g)
{
    assert(g != NULL);

    hashmap_neighbours_foreach(g->neighbours, graph_adhoc_edges_destroyer);
    hashmap_neighbours_destroy(g->neighbours);

    free(g);
}

size_t graph_vertices(graph_t g)
{
    assert(g != NULL);

    return hashmap_neighbours_size(g->neighbours);
}

size_t graph_edges(graph_t g)
{
    assert(g != NULL);

    return g->edges;
}

bool graph_vertex_exists(graph_t g, u32 v)
{
    assert(g != NULL);

    return hashmap_neighbours_find(g->neighbours, v) != NULL;
}

/**
 * Verify the directed edge 'x' to 'y' was properly added to the graph.
 * Return true if the edge exists, otherwise false.
 *
 * Pre: g != NULL && the edge exists in both forward and backward maps or in
 *      neither
 */
static bool graph_verify_edge_exists(graph_t g, u32 x, u32 y)
{
    neighbours_t ngb = NULL;
    bool forward = false, backward = false;

    assert(g != NULL);

    /* check x's forward table */
    ngb = hashmap_neighbours_find(g->neighbours, x);
    if (ngb != NULL && hashmap_edge_find(ngb->forward, y) != NULL)
        forward = true;

    /* check y's backward table */
    ngb = hashmap_neighbours_find(g->neighbours, y);
    if (ngb != NULL && hashmap_edge_find(ngb->backward, x) != NULL)
        backward = true;

    assert(forward == backward);

    return forward && backward;
}

bool graph_edge_exists(graph_t g, u32 x, u32 y)
{
    assert(g != NULL);
    assert(graph_vertex_exists(g, x));
    assert(graph_vertex_exists(g, y));

    return graph_verify_edge_exists(g, x, y);
}

/**
 * Add a vertex 'v' to the graph, if it doesn't exist yet. Otherwise do
 * nothing.
 * Return the index corresponding the vertex (newly added or existent).
 *
 * Pre: g != NULL
 */
static void graph_add_vertex(graph_t g, u32 v)
{
    neighbours_t ngb = NULL;

    assert(g != NULL);

    /* if already exists, do nothing else */
    if ((ngb = hashmap_neighbours_find(g->neighbours, v)))
        return;

    ngb = hashmap_neighbours_at(g->neighbours, v);

    /* create edge tables */
    ngb->forward  = hashmap_edge_create(u32_hash, INITIAL_EDGES_ALLOC);
    ngb->backward = hashmap_edge_create(u32_hash, INITIAL_EDGES_ALLOC);
}

void graph_new_vertex(graph_t g, u32 v)
{
    assert(g != NULL);
    assert(!graph_vertex_exists(g, v));

    graph_add_vertex(g, v);
}

void graph_new_directed_edge(graph_t g, u32 x, u32 y, u32 cap)
{
    edge_t edge = NULL;
    neighbours_t ngb = NULL;

    assert(g != NULL);
    /* note: the following is not the same as graph_edge_exists(g, x, y) */
    assert(!graph_verify_edge_exists(g, x, y));

    graph_add_vertex(g, x);
    graph_add_vertex(g, y);

    edge = edge_create(cap);

    /* add to x's forward table */
    ngb = hashmap_neighbours_at(g->neighbours, x);
    *hashmap_edge_at(ngb->forward, y) = edge;

    /* add to y's backward table */
    ngb = hashmap_neighbours_at(g->neighbours, y);
    *hashmap_edge_at(ngb->backward, x) = edge;

    ++g->edges;
}

edge_t graph_directed_edge(graph_t g, u32 x, u32 y)
{
    neighbours_t ngb = NULL;

    assert(g != NULL);
    assert(graph_edge_exists(g, x, y));

    ngb = hashmap_neighbours_find(g->neighbours, x);
    return *hashmap_edge_find(ngb->forward, y);
}

bool graph_foreach_vertex(graph_t g, graph_foreach_vertex_f f, void* data)
{
    bool stop = false;
    hashmap_neighbours_iterator_t it;
    u32 v;

    assert(g != NULL);
    assert(f != NULL);

    for (it = hashmap_neighbours_iterator_begin(g->neighbours);
                !stop && !hashmap_neighbours_iterator_end(g->neighbours, it);
                    it = hashmap_neighbours_iterator_next(g->neighbours, it)) {
        v = *hashmap_neighbours_iterator_key(g->neighbours, it);
        stop = f(v, data);
    }

    return stop;
}

/**
 * Call 'f' for each vertex in 'edges' (neighbours of 'v'). Pass 'data' along.
 * Stop the iteration if 'f' returns true.
 * Return true if the iteration was stopped.
 *
 * Pre: edges != NULL && f != NULL
 */
static bool graph_foreach_edge_neighbours(hashmap_edge_t edges, u32 v,
        graph_foreach_edge_f f, void* data)
{
    bool stop = false;
    edge_t e = NULL;
    hashmap_edge_iterator_t it;
    u32 z;

    assert(edges != NULL);
    assert(f != NULL);

    for (it = hashmap_edge_iterator_begin(edges);
                    !stop && !hashmap_edge_iterator_end(edges, it);
                                it = hashmap_edge_iterator_next(edges, it)) {
        z = *hashmap_edge_iterator_key(edges, it);
        e = *hashmap_edge_iterator_at(edges, it);
        stop = f(v, z, e, data);
    }

    return stop;
}

bool graph_foreach_edge(graph_t g, graph_foreach_edge_f f, void* data)
{
    bool stop = false;
    hashmap_neighbours_iterator_t it;
    u32 v;

    assert(g != NULL);
    assert(f != NULL);

    for (it = hashmap_neighbours_iterator_begin(g->neighbours);
                !stop && !hashmap_neighbours_iterator_end(g->neighbours, it);
                    it = hashmap_neighbours_iterator_next(g->neighbours, it)) {
        v = *hashmap_neighbours_iterator_key(g->neighbours, it);
        stop = graph_foreach_edge_forward(g, v, f, data);
    }

    return stop;
}

bool graph_foreach_edge_forward(graph_t g, u32 v, graph_foreach_edge_f f,
        void* data)
{
    neighbours_t ngb = NULL;

    assert(g != NULL);
    assert(f != NULL);
    assert(graph_vertex_exists(g, v));

    ngb = hashmap_neighbours_find(g->neighbours, v);

    return graph_foreach_edge_neighbours(ngb->forward, v, f, data);
}

bool graph_foreach_edge_backward(graph_t g, u32 v, graph_foreach_edge_f f,
        void* data)
{
    neighbours_t ngb = NULL;

    assert(g != NULL);
    assert(f != NULL);
    assert(graph_vertex_exists(g, v));

    ngb = hashmap_neighbours_find(g->neighbours, v);

    return graph_foreach_edge_neighbours(ngb->backward, v, f, data);
}

/**
 * Left-fold 'f' over the vertices in 'edges' (neighbours of 'v') using 'base'
 * as initial partial result. Pass 'data' along to 'f'.
 * Return the result of the folding.
 *
 * Pre: edges != NULL && f != NULL
 */
static u64 graph_foldl_edge_neighbours(hashmap_edge_t edges, u32 v,
        graph_foldl_edge_f f, u64 base, void* data)
{
    edge_t e = NULL;
    hashmap_edge_iterator_t it;
    u32 z;

    assert(edges != NULL);
    assert(f != NULL);

    for (it = hashmap_edge_iterator_begin(edges);
                    !hashmap_edge_iterator_end(edges, it);
                                it = hashmap_edge_iterator_next(edges, it)) {
        z = *hashmap_edge_iterator_key(edges, it);
        e = *hashmap_edge_iterator_at(edges, it);
        base = f(v, z, e, base, data);
    }

    return base;
}

u64 graph_foldl_edge_forward(graph_t g, u32 v, graph_foldl_edge_f f,
        u64 base, void* data)
{
    neighbours_t ngb = NULL;

    assert(g != NULL);
    assert(f != NULL);
    assert(graph_vertex_exists(g, v));

    ngb = hashmap_neighbours_find(g->neighbours, v);

    return graph_foldl_edge_neighbours(ngb->forward, v, f, base, data);
}

u64 graph_foldl_edge_backward(graph_t g, u32 v, graph_foldl_edge_f f,
        u64 base, void* data)
{
    neighbours_t ngb = NULL;

    assert(g != NULL);
    assert(f != NULL);
    assert(graph_vertex_exists(g, v));

    ngb = hashmap_neighbours_find(g->neighbours, v);

    return graph_foldl_edge_neighbours(ngb->backward, v, f, base, data);
}
