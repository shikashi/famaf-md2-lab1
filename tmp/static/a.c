#include <stdio.h>

static void a_f(int);

void a_f(int i)
{
    printf("a_f(%d)\n", i);
}

void a_s(const char* s)
{
    printf("a_s(%s)\n", s);
}
