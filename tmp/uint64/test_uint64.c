#include <stdlib.h>
#include <stdio.h>

#if USE_STD
#  include <stdint.h>
#  include <limits.h>
#endif /* USE_STD */

#if USE_PORTABLE
#  include "pstdint.h"
#endif /* USE_PORTABLE */

#include <sys/types.h>

uint32_t u32_a = UINT32_MAX;
uint64_t u64_b = UINT64_MAX;

__uint64_t _u64_c = UINT64_MAX;

int main()
{
    printf("u32_a: %lu\n", sizeof(u32_a));
    printf("u64_b: %lu\n", sizeof(u64_b));
    printf("_u64_c: %lu\n", sizeof(_u64_c));

    return 0;
}
